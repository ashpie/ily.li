document.addEventListener('DOMContentLoaded', () => {
    const menuButton = document.getElementById('menu-button');
    const mainMenu = document.getElementById('main-menu');

    menuButton.addEventListener('click', (e) => {
        e.stopPropagation();
        mainMenu.classList.toggle('open');
    });

    mainMenu.addEventListener('click', (e) => {
        e.stopPropagation();
    });

    document.addEventListener('click', () => {
        mainMenu.classList.remove('open');
    });
});