document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('url-shrink-form');
    if (!form) return;

    const autogenUrlCheckbox = document.getElementById('field-autogen_url');
    const slugField = document.getElementById('field-slug');

    autogenUrlCheckbox.addEventListener('change', () => {
        slugField.disabled = autogenUrlCheckbox.checked;
    });
});
