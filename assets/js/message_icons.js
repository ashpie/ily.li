import feather from "feather-icons";

document.addEventListener('DOMContentLoaded', () => {
    const messageTypeToIcon = {
        info: 'info',
        success: 'check',
        warning: 'alert-triangle',
        error: 'x-circle',
        question: 'help-circle',
    };
    document.querySelectorAll('.message').forEach(el => {
        const type = el.dataset['type'];
        const icon = el.querySelector('.icon');
        const svgContainer = document.createElement('div');
        svgContainer.innerHTML = feather.icons[messageTypeToIcon[type]].toSvg();
        el.insertBefore(svgContainer.firstChild, icon);
        icon.remove();
    });

    feather.replace();
});