document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('upload-form');
    if (!form) return;
    const upload = document.getElementById('file-upload');
    const uploadLink = document.getElementById('file-upload-link');

    const uploadField = document.getElementById('field-upload');

    const neverExpireCheckbox = document.getElementById('field-never_expire');
    const expireAfterDaysField = document.getElementById('field-expire_after_days');

    const autogenUrlCheckbox = document.getElementById('field-autogen_url');
    const slugField = document.getElementById('field-slug');

    neverExpireCheckbox.addEventListener('change', () => {
        expireAfterDaysField.disabled = neverExpireCheckbox.checked;
    });

    autogenUrlCheckbox.addEventListener('change', () => {
        slugField.disabled = autogenUrlCheckbox.checked;
    });

    let uploadForm;

    form.addEventListener('submit', e => {
        e.preventDefault();
        if (!uploadForm || uploadForm.isFinished()) {
            uploadForm = new UploadForm(form, upload, uploadLink, uploadField.files[0].name);
            uploadForm.updateView();
            uploadForm.start();
        }
    });
});

function UploadForm(form, upload, uploadLink, fileName) {
    this.form = form;
    this.upload = upload;
    this.uploadLink = uploadLink;
    this.fileName = fileName;
    this.progressBar = this.upload.querySelector('.progress-bar');
    this.progressBarContent = this.progressBar.querySelector('.content');
    this.status = this.upload.querySelector('.status');
    this.speed = this.status.querySelector('.speed');
    this.finished = false;

    this.xferSpeed = [];
    this.lastTransferTime = null;

    this.xhr = new XMLHttpRequest();
    this.xhr.responseType = 'json';
    this.xhr.upload.addEventListener('progress', e => {
        if (e.lengthComputable) {
            this.progressBar.classList.remove('undefined');
            let percent = ((e.loaded / e.total) * 100).toFixed(2) + '%';
            this.progressBar.style = `--progress: ${percent}`;
            this.progressBarContent.innerText = percent;
            this.updateSpeed(e.loaded);
        } else {
            this.progressBar.classList.add('undefined');
        }
    });
    this.xhr.upload.addEventListener('loadstart', () => {
        this.status.classList.remove('hidden');
    });
    this.xhr.addEventListener('load', () => {
        this.finished = true;
        let response = this.xhr.response;
        console.log('done', response);
        if (response.status === 'error') {
            if (response.messages) {
                this.restoreView();
                window.applyFormMessages(this.form, response.messages);
            }
        } else if (response.url) {
            this.status.innerHTML = 'Done!';
            this.uploadLink.querySelector('.content').innerText = response.url;
            this.uploadLink.classList.remove('hidden');
        } else {
            window.location.reload();
        }
    });
    this.xhr.addEventListener('error', (e) => {
        this.finished = true;
        console.error('error', e);
        this.status.innerHTML = 'Error; upload was interrupted.';
    });
}

UploadForm.prototype.isFinished = function () {
    return this.finished;
}

UploadForm.prototype.updateView = function () {
    this.upload.querySelector('.name').innerText = this.fileName;
    this.form.classList.add('hidden');
    this.upload.classList.remove('hidden');
    this.status.innerHTML = `Uploading @ <span class="speed">--</span>`;
    this.speed = this.status.querySelector('.speed');
}

UploadForm.prototype.restoreView = function () {
    this.status.classList.add('hidden');
    this.upload.classList.add('hidden');
    this.form.classList.remove('hidden');
};

UploadForm.prototype.start = function () {
    const formData = new FormData(this.form);
    this.xhr.open('POST', this.form.action);
    this.xhr.send(formData);
}

const units = ['K', 'M', 'G', 'T'];
UploadForm.prototype.updateSpeed = function (loaded) {
    const time = new Date().getTime();
    if (this.lastTransferTime) {
        this.xferSpeed.push((loaded - this.lastLoaded) / (time - this.lastTransferTime));

        if (this.xferSpeed.length > 100) this.xferSpeed = this.xferSpeed.slice(1);

        let speed = this.xferSpeed.reduce((v, c) => v + c) / this.xferSpeed.length;
        let unit = 0;
        while (speed >= 1000 && unit < units.length - 1) {
            speed /= 1000;
            unit++;
        }
        this.speed.innerText = (speed).toFixed(2) + units[unit] + 'Bps';
    }
    this.lastTransferTime = time;
    this.lastLoaded = loaded;
}
