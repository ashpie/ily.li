#!/bin/sh

target_url=$1
slug=$2
token=$(cat "$HOME/.ily_token")

if test -z "$slug"; then
  method=POST
else
  method=PUT
fi

curl -X $method -H 'Accept: text/plain' \
  -H "Authorization: $token" \
  -F 'type=url' \
  -F "target_url=$target_url" \
  "https://ily.li/$slug"
