export default Object.assign(require("wms-core/config/default").default, {
    app: {
        name: 'ily.li',
        contact_email: 'contact@ily.li'
    },
    domain: 'localhost',
    base_url: 'http://localhost:4893',
    public_websocket_url: 'ws://localhost:4893',
    port: 4893,
    mysql: {
        connectionLimit: 10,
        host: "localhost",
        user: "root",
        password: "",
        database: "ilyli",
        create_database_automatically: false
    },
    magic_link: {
        validity_period: 20
    },
    newlyGeneratedSlugSize: 3,
    default_file_ttl: 30, // 30 seconds
    max_upload_size: 1, // MB
    approval_mode: false,
    mail: {
        host: "127.0.0.1",
        port: "1025",
        secure: false,
        username: "",
        password: "",
        allow_invalid_tls: true,
        from: 'contact@ily.li',
        from_name: 'ily.li',
    },
    allowed_url_domains: [
        'localhost:4893',
        '127.0.0.1:4893',
    ],
    default_url_domain_for_files: 0,
    default_url_domain_for_urls: 1,
});