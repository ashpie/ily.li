export default Object.assign(require("wms-core/config/production").default, {
    domain: 'ily.li',
    base_url: 'https://ily.li',
    public_websocket_url: 'wss://ily.li',
    mysql: {
        connectionLimit: 10,
        host: "localhost",
        user: "root",
        password: "",
        database: "ilyli",
    },
    magic_link: {
        validity_period: 900
    },
    newlyGeneratedSlugSize: 5,
    default_file_ttl: 30 * 24 * 3600, // 30 days
    max_upload_size: 8192, // MB
    approval_mode: true,
    mail: {
        secure: true,
        allow_invalid_tls: false
    },
    allowed_url_domains: [
        'ily.li',
        'gris.li',
    ],
    default_url_domain_for_files: 0,
    default_url_domain_for_urls: 1,
});