export default Object.assign(require("wms-core/config/test").default, {
    mysql: {
        database: "ilyli_test",
        create_database_automatically: true
    },
    magic_link: {
        validity_period: 2
    },
});