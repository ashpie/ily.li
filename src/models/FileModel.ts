import Model from "wms-core/db/Model";
import Controller from "wms-core/Controller";
import config from "config";
import User from "wms-core/auth/models/User";
import {Request} from "express";
import URLRedirect from "./URLRedirect";

export default class FileModel extends Model {
    public static get table(): string {
        return 'files';
    }

    public static async getBySlug(slug: string): Promise<FileModel | null> {
        return await this.select().where('slug', slug).first();
    }

    public static async paginateForUser(req: Request, perPage: number, user_id: number): Promise<FileModel[]> {
        return await this.paginate(req, perPage, this.select().where('user_id', user_id));
    }

    public id?: number = undefined;
    public readonly user_id?: number = undefined;
    public readonly slug?: string = undefined;
    public readonly real_name?: string = undefined;
    public readonly storage_type?: FileStorage = undefined;
    public readonly storage_path?: string = undefined;
    public readonly size?: number = undefined;
    public created_at?: Date = undefined;
    public readonly ttl?: number = undefined;

    protected init() {
        this.setValidation('user_id').defined().exists(User, 'id');
        this.setValidation('slug').defined().minLength(1).maxLength(259).unique(FileModel, 'slug').unique(URLRedirect, 'slug');
        this.setValidation('real_name').defined().minLength(1).maxLength(259);
        this.setValidation('storage_type').defined().maxLength(64);
        this.setValidation('storage_path').defined().maxLength(1745);
        this.setValidation('size').defined().min(0);
        this.setValidation('ttl').defined().min(0).max(4294967295);
    }

    public getURL(domain: string = config.get<string>('base_url')): string {
        return (/^https?:\/\//.test(domain) ? '' : 'https://') + domain + Controller.route('get-file', {
            slug: this.slug!,
        });
    }

    public getExpirationDate(): Date | null {
        if (!this.created_at) return new Date();
        if (this.ttl === 0) return null;

        return new Date(this.created_at.getTime() + this.ttl! * 1000);
    }

    public shouldBeDeleted(): boolean {
        const expirationDate = this.getExpirationDate();
        if (!expirationDate) return false;
        return new Date().getTime() >= expirationDate.getTime();
    }

    public canDelete(user_id: number): boolean {
        return this.user_id === user_id;
    }
}

export type FileStorage = 'local';
