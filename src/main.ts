import Logger from "wms-core/Logger";
import App from "./App";
import config from "config";

(async () => {
    const app = new App(config.get<number>('port'));
    await app.start();
})().catch(err => {
    Logger.error(err);
});