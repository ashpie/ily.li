import Controller from "wms-core/Controller";
import {REQUIRE_AUTH_MIDDLEWARE} from "wms-core/auth/AuthComponent";
import {NextFunction, Request, Response} from "express";
import {BadRequestError, ForbiddenHttpError, ServerError} from "wms-core/HttpError";
import FileModel from "../models/FileModel";
import config from "config";
import * as fs from "fs";
import AuthToken from "../models/AuthToken";
import {IncomingForm} from "formidable";
import generateSlug from "../SlugGenerator";
import Logger from "wms-core/Logger";
import FileUploadMiddleware from "wms-core/FileUploadMiddleware";


export default class FileController extends Controller {
    routes(): void {
        this.get('/files/upload', this.getFileUploader, 'file-upload', REQUIRE_AUTH_MIDDLEWARE);
        this.get('/files/upload/script', this.downloadLinuxScript, 'file-linux-script');
        this.post('/files/post', this.postFileFrontend, 'post-file-frontend', REQUIRE_AUTH_MIDDLEWARE, FILE_UPLOAD_FORM_MIDDLEWARE);
        this.get('/files/:page([0-9]+)?', this.getFileManager, 'file-manager', REQUIRE_AUTH_MIDDLEWARE);
        this.post('/files/delete/:slug', FileController.deleteFileRoute, 'delete-file-frontend', REQUIRE_AUTH_MIDDLEWARE);
    }

    protected async getFileUploader(req: Request, res: Response): Promise<void> {
        const allowedDomains = config.get<string[]>('allowed_url_domains');
        res.render('file-upload', {
            max_upload_size: config.get<string>('max_upload_size'),
            auth_tokens: await AuthToken.select().where('user_id', req.models.user!.id!).get(),
            allowed_domains: allowedDomains,
            default_domain: allowedDomains[config.get<number>('default_url_domain_for_files')],
        });
    }

    protected async downloadLinuxScript(req: Request, res: Response): Promise<void> {
        res.download('assets/files/upload_file.sh', 'upload_file.sh');
    }

    protected async getFileManager(req: Request, res: Response): Promise<void> {
        res.render('file-manager', {
            files: await FileModel.paginateForUser(req, 100, req.models.user!.id!),
        });
    }

    protected async postFileFrontend(req: Request, res: Response): Promise<void> {
        req.body.type = 'file';
        await FileController.handleFileUpload(req.body.autogen_url === undefined && req.body.slug ? req.body.slug : await generateSlug(10), req, res);
    }

    public static async handleFileUpload(slug: string, req: Request, res: Response): Promise<void> {
        // Check for file upload
        if (!req.files || !req.files['upload']) {
            throw new BadRequestError('No file received.', 'You must upload exactly one (1) file.', req.url);
        }

        let upload = req.files['upload'];

        // TTL
        let ttl = config.get<number>('default_file_ttl');
        if (req.body.never_expire !== undefined) ttl = 0;
        if (req.body.ttl !== undefined) ttl = parseInt(req.body.ttl);
        else if (req.body.expire_after_days !== undefined) ttl = parseInt(req.body.expire_after_days) * 24 * 3600;

        const file = FileModel.create({
            user_id: req.models.user!.id,
            slug: slug,
            real_name: upload.name,
            storage_type: 'local',
            storage_path: 'storage/uploads/' + slug,
            size: upload.size,
            ttl: ttl,
        });

        await file.save();
        fs.renameSync(upload.path, file.storage_path!);

        const domain = req.body.url_domain || config.get<string[]>('allowed_url_domains')[config.get<number>('default_url_domain_for_files')];
        res.format({
            json: () => res.json({
                url: file.getURL(domain),
            }),
            text: () => res.send(file.getURL(domain)),
            html: () => {
                req.flash('success', 'Upload success!');
                req.flash('url', file.getURL(domain));
                res.redirectBack('/');
            },
        });
    }

    public static async deleteFileRoute(req: Request, res: Response, next: NextFunction): Promise<void> {
        const slug = req.params.slug;
        if (!slug) throw new BadRequestError('Cannot delete nothing.', 'Please provide a slug.', req.url);

        const file = await FileModel.getBySlug(req.params.slug);
        if (!file) return next();
        if (!file.canDelete(req.models.user!.id!)) throw new ForbiddenHttpError('file', req.url);

        switch (file.storage_type) {
            case 'local':
                await FileController.deleteFile(file);
                break;
            default:
                throw new ServerError(`This file cannot be deleted. Deletion protocol for ${file.storage_type} storage type not implemented.`);
        }

        res.format({
            json: () => res.json({
                status: 'success',
            }),
            text: () => res.send('success'),
            html: () => {
                req.flash('success', 'Successfully deleted file.');
                res.redirectBack('/');
            },
        });
    }

    public static async deleteFile(file: FileModel): Promise<void> {
        fs.unlinkSync(file.storage_path!);
        await file.delete();
        Logger.info('Deleted', file.storage_path, `(${file.real_name})`);
    }
}

export const FILE_UPLOAD_FORM_MIDDLEWARE = new FileUploadMiddleware(() => {
    const form = new IncomingForm();
    form.uploadDir = 'storage/tmp';
    form.maxFileSize = config.get<number>('max_upload_size') * 1024 * 1024;
    return form;
}, 'upload');
