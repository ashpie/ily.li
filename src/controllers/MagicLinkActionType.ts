import MagicLink from "wms-core/auth/models/MagicLink";

export enum MagicLinkActionType {
    LOGIN = 'Login',
    REGISTER = 'Register',
}

export function getActionMessage(magicLink: MagicLink): string {
    switch (magicLink.getActionType()) {
        case MagicLinkActionType.LOGIN:
            return 'You have been authenticated.';
        case MagicLinkActionType.REGISTER:
            return 'You have been registered.';
    }

    return '';
}