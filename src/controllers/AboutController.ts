import Controller from "wms-core/Controller";
import {Request, Response} from "express";

export default class AboutController extends Controller {
    routes(): void {
        this.get('/', this.getAbout, 'home');
        this.get('/', this.getAbout, 'about');
    }

    private async getAbout(req: Request, res: Response) {
        res.render('about');
    }
}