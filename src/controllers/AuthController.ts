import MagicLinkAuthController from "wms-core/auth/magic_link/MagicLinkAuthController";
import {MAGIC_LINK_MAIL} from "wms-core/Mails";

export default class AuthController extends MagicLinkAuthController {
    public constructor() {
        super(MAGIC_LINK_MAIL);
    }
}