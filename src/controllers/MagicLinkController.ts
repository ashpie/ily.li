import _MagicLinkController from "wms-core/auth/magic_link/MagicLinkController";
import {Request, Response} from "express";
import Controller from "wms-core/Controller";
import MagicLinkWebSocketListener from "wms-core/auth/magic_link/MagicLinkWebSocketListener";
import MagicLink from "wms-core/auth/models/MagicLink";
import AuthController from "./AuthController";
import {MagicLinkActionType} from "./MagicLinkActionType";

export default class MagicLinkController extends _MagicLinkController {
    constructor(magicLinkWebSocketListener: MagicLinkWebSocketListener) {
        super(magicLinkWebSocketListener);
    }

    protected async performAction(magicLink: MagicLink, req: Request, res: Response): Promise<void> {
        switch (magicLink.getActionType()) {
            case MagicLinkActionType.LOGIN:
            case MagicLinkActionType.REGISTER:
                await AuthController.checkAndAuth(req, res, magicLink);
                const proof = await req.authGuard.isAuthenticated(req.session!);
                const user = await proof?.getResource();

                if (!res.headersSent && user) {
                    // Auth success
                    req.flash('success', `Authentication success. Welcome, ${user.name}!`);
                    res.redirect(req.query.redirect_uri?.toString() || Controller.route('home'));
                }
                break;
        }
    }
}