import Controller from "wms-core/Controller";
import {NextFunction, Request, Response} from "express";
import {BadRequestError, ForbiddenHttpError, NotFoundHttpError, ServerError} from "wms-core/HttpError";
import config from "config";
import {REQUIRE_REQUEST_AUTH_MIDDLEWARE} from "wms-core/auth/AuthComponent";
import URLRedirect from "../models/URLRedirect";
import URLRedirectController from "./URLRedirectController";
import FileModel from "../models/FileModel";
import generateSlug from "../SlugGenerator";
import FileController, {FILE_UPLOAD_FORM_MIDDLEWARE} from "./FileController";
import * as fs from "fs";
import {encodeRFC5987ValueChars} from "../Utils";

export default class LinkController extends Controller {
    routes(): void {
        this.post('/', this.postFile, 'post-file', REQUIRE_REQUEST_AUTH_MIDDLEWARE, FILE_UPLOAD_FORM_MIDDLEWARE);
        this.delete('/:slug', FileController.deleteFileRoute, 'delete-file', REQUIRE_REQUEST_AUTH_MIDDLEWARE);
        this.get('/:slug', this.getFile, 'get-file');
        this.put('/:slug', this.putFile, 'put-file', REQUIRE_REQUEST_AUTH_MIDDLEWARE, FILE_UPLOAD_FORM_MIDDLEWARE);

        this.post('/', URLRedirectController.addURL, 'post-url', REQUIRE_REQUEST_AUTH_MIDDLEWARE);
        this.delete('/:slug', this.deleteURL, 'delete-url', REQUIRE_REQUEST_AUTH_MIDDLEWARE);
        this.get('/:slug', this.getURLRedirect, 'get-url');
        this.put('/:slug', URLRedirectController.addURL, 'put-url', REQUIRE_REQUEST_AUTH_MIDDLEWARE);

        this.get(/(.*)/, this.domainFilter);
    }

    protected async getFile(req: Request, res: Response, next: NextFunction): Promise<void> {
        console.log('get file', req.params.slug)
        const file = await FileModel.getBySlug(req.params.slug);
        if (!file) return next();
        if (file.shouldBeDeleted()) {
            await FileController.deleteFile(file);
            return next();
        }

        // File type
        const fileName = file.real_name!;
        const parts = fileName.split('.');
        res.type(parts[parts.length - 1]);

        // File name
        res.header('Content-Disposition', `inline; filename*=UTF-8''${encodeRFC5987ValueChars(fileName)}`);

        switch (file.storage_type) {
            case 'local':
                fs.readFile(file.storage_path!, (err, data) => {
                    if (err) return next(err);
                    res.send(data);
                });
                break;
            default:
                throw new ServerError(`This file cannot be served. Download protocol for ${file.storage_type} storage type not implemented.`);
        }
    }

    protected async postFile(req: Request, res: Response, next: NextFunction): Promise<void> {
        if (req.body.type !== 'file') return next();

        await FileController.handleFileUpload(req.body.slug || await generateSlug(10), req, res);
    }

    protected async putFile(req: Request, res: Response, next: NextFunction): Promise<void> {
        if (req.body.type !== 'file') return next();
        const slug = req.params.slug;
        if (!slug) throw new BadRequestError('Cannot put without a slug.', 'Either provide a slug or use POST method instead.', req.url);

        await FileController.handleFileUpload(slug, req, res);
    }

    protected async getURLRedirect(req: Request, res: Response, next: NextFunction): Promise<void> {
        const url = await URLRedirect.getBySlug(req.params.slug);
        if (!url) return next();

        res.redirect(url.target_url!, 301);
    }

    protected async deleteURL(req: Request, res: Response, next: NextFunction): Promise<void> {
        const urlRedirect = await URLRedirect.getBySlug(req.params.slug);
        if (!urlRedirect) return next();

        throw new BadRequestError(
            'Deleting url redirects is disabled for security reasons.',
            'If you still want to disable the redirection, please contact us via email.',
            req.url
        );
    }

    protected async domainFilter(req: Request, res: Response, next: NextFunction): Promise<void> {
        if (req.hostname !== config.get('domain')) {
            if (req.path === '/') return res.redirect(config.get<string>('base_url'));
            throw new NotFoundHttpError('Page', req.url);
        }
        next();
    }
}