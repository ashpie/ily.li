import Controller from "wms-core/Controller";
import {REQUIRE_AUTH_MIDDLEWARE} from "wms-core/auth/AuthComponent";
import {Request, Response} from "express";
import AuthToken from "../models/AuthToken";
import {BadRequestError, ForbiddenHttpError, NotFoundHttpError} from "wms-core/HttpError";

export default class AuthTokenController extends Controller {
    routes(): void {
        this.post('/gen-auth-token', this.postGenAuthToken, 'generate-token', REQUIRE_AUTH_MIDDLEWARE);
        this.post('/revoke-auth-token/:id', this.postRevokeAuthToken, 'revoke-token', REQUIRE_AUTH_MIDDLEWARE);
    }

    protected async postGenAuthToken(req: Request, res: Response): Promise<void> {
        const authToken = AuthToken.create({
            user_id: req.models.user!.id,
            ttl: req.body.ttl ? parseInt(req.body.ttl) : 365 * 24 * 3600,
        });
        await authToken.save();
        req.flash('success', 'Successfully created auth token.');
        res.redirectBack(Controller.route('file-upload'));
    }

    protected async postRevokeAuthToken(req: Request, res: Response): Promise<void> {
        const id = req.params.id;
        if (!id) throw new BadRequestError('Cannot revoke token without an id.', 'Please provide an id.', req.url);

        const authToken = await AuthToken.getById<AuthToken>(parseInt(id));
        if (!authToken) throw new NotFoundHttpError('Auth token', req.url);
        if (!authToken.canDelete(req.models.user!.id!)) throw new ForbiddenHttpError('auth token', req.url);

        await authToken.delete();

        req.flash('success', 'Successfully deleted auth token.');
        res.redirectBack(Controller.route('file-upload'));
    }
}