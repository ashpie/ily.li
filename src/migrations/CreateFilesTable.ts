import {Connection} from "mysql";
import Migration from "wms-core/db/Migration";
import ModelFactory from "wms-core/db/ModelFactory";
import FileModel from "../models/FileModel";

export default class CreateFilesTable extends Migration {
    public async install(connection: Connection): Promise<void> {
        await this.query(`CREATE TABLE files
                          (
                              id           INT                 NOT NULL AUTO_INCREMENT,
                              user_id      INT                 NOT NULL,
                              slug         VARCHAR(259) UNIQUE NOT NULL,
                              real_name    VARCHAR(259)        NOT NULL,
                              storage_type VARCHAR(64)         NOT NULL,
                              storage_path VARCHAR(1745)       NOT NULL,
                              size         INT UNSIGNED        NOT NULL,
                              created_at   DATETIME            NOT NULL DEFAULT NOW(),
                              ttl          INT UNSIGNED        NOT NULL,
                              PRIMARY KEY (id)
                          )`, connection);
    }

    public async rollback(connection: Connection): Promise<void> {
        await this.query(`DROP TABLE IF EXISTS files`, connection);
    }

    public registerModels(): void {
        ModelFactory.register(FileModel);
    }
}