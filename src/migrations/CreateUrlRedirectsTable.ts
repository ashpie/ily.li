import Migration from "wms-core/db/Migration";
import {Connection} from "mysql";
import ModelFactory from "wms-core/db/ModelFactory";
import URLRedirect from "../models/URLRedirect";

export default class CreateUrlRedirectsTable extends Migration {
    public async install(connection: Connection): Promise<void> {
        await this.query(`CREATE TABLE url_redirects
                          (
                              id         INT                 NOT NULL AUTO_INCREMENT,
                              user_id    INT                 NOT NULL,
                              slug       VARCHAR(259) UNIQUE NOT NULL,
                              target_url VARCHAR(1745)       NOT NULL,
                              created_at DATETIME            NOT NULL DEFAULT NOW(),
                              PRIMARY KEY (id)
                          )`, connection);
    }

    public async rollback(connection: Connection): Promise<void> {
        await this.query(`DROP TABLE IF EXISTS url_redirects`, connection);
    }

    public registerModels(): void {
        ModelFactory.register(URLRedirect);
    }
}