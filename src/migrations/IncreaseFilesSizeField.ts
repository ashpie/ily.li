import Migration from "wms-core/db/Migration";
import {Connection} from "mysql";

export default class IncreaseFilesSizeField extends Migration {
    public async install(connection: Connection): Promise<void> {
        await this.query(`ALTER TABLE files MODIFY size BIGINT UNSIGNED`, connection);
    }

    public async rollback(connection: Connection): Promise<void> {
        await this.query(`ALTER TABLE files MODIFY size INT UNSIGNED`, connection);
    }

    public registerModels(): void {
    }
}