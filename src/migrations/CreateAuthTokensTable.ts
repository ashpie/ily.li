import {Connection} from "mysql";
import Migration from "wms-core/db/Migration";
import ModelFactory from "wms-core/db/ModelFactory";
import AuthToken from "../models/AuthToken";

export default class CreateAuthTokensTable extends Migration {
    public async install(connection: Connection): Promise<void> {
        await this.query(`CREATE TABLE auth_tokens
                          (
                              id         INT                NOT NULL AUTO_INCREMENT,
                              user_id    INT                NOT NULL,
                              secret     VARCHAR(64) UNIQUE NOT NULL,
                              created_at DATETIME           NOT NULL DEFAULT NOW(),
                              used_at    DATETIME           NOT NULL DEFAULT NOW(),
                              ttl        INT UNSIGNED       NOT NULL,
                              PRIMARY KEY (id)
                          )`, connection);
    }

    public async rollback(connection: Connection): Promise<void> {
        await this.query(`DROP TABLE IF EXISTS auth_tokens`, connection);
    }

    public registerModels(): void {
        ModelFactory.register(AuthToken);
    }

}